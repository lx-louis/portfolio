import styles from '../../styles/Home.module.css'
import Link from 'next/link'

export default function HeroProjects(){
    return (
        <div className=' snap-start w-screen h-screen flex flex-col items-center justify-center'>
          <div className="flex flex-row justify-between items-center py-8 border-solid border-b border-slate-400 w-10/12 fixed top-0">
            <div className='flex justify-center align-center items-center space-x-2'>
              <Link href='/' className='text-xl '>
                  <a>Louis Garrido</a>
              </Link>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-cyan-500" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
              </svg>
            </div>
            <Link className='text-xlbg-sky-500/25 p-2 rounded-md ' href='/project'>
              <a className='transition ease-in-out duration-500 hover:scale-125 underline decoration-cyan-300 underline-offset-4 decoration-2'>Projects</a>
            </Link>
          </div>
          <div className='lg:flex lg:flex-row flex flex-col justify-between'>
              <Link href='/Web2Projects'>
                <a className="bg-gradient-to-r from-cyan-400 to-blue-400 text-3xl text-center text-white border border-solid rounded-full lg:py-16 py-12 lg:px-56 px-24 mb-4 lg:mb-0 lg:mr-8 transition ease-in-out duration-500 hover:scale-105">Web2</a>
              </Link>
              <Link href='/Web3Projects'>
                <a className="bg-gradient-to-r from-blue-400 to-indigo-500 text-3xl text-center text-white border border-solid rounded-full lg:py-16 py-12 lg:px-56 px-24 lg:ml-8 transition ease-in-out duration-500 hover:scale-105 mx-0">Web3</a>
              </Link>
          </div>
        </div>
    )
}