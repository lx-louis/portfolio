export default function Countries(props){
    return(
        <div className='flex items-start h-28 mt-4'>
            <div className="w-full h-full rounded-lg bg-emerald-300">
                <div className="m-2 flex flex-col">
                    <div className="text-white font-extrabold lg:text-sm text-xs pb-2">
                        {props.country}
                    </div>
                    <div className="text-white font-bold text-xs">
                        {props.reason}
                    </div>
                </div>
            </div>
        </div>
    )
}