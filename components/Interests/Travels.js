import Countries from './Countries'
import { useRouter } from 'next/router';
import indexEN from '../../i18n/en/index.json'
import indexFR from '../../i18n/fr/index.json'
export default function Travels(){
    let router = useRouter();
    let h3 = router.locale === 'en'? indexEN.travels
    : router.locale === 'fr'
    ? indexFR.travels
    : "";
    let c1_c = router.locale === 'en'? indexEN.c1_c
    : router.locale === 'fr'
    ? indexFR.c1_c
    : "";
    let c1_r = router.locale === 'en'? indexEN.c1_r
    : router.locale === 'fr'
    ? indexFR.c1_r
    : "";
    let c2_c = router.locale === 'en'? indexEN.c2_c
    : router.locale === 'fr'
    ? indexFR.c1_c
    : "";
    let c2_r = router.locale === 'en'? indexEN.c2_r
    : router.locale === 'fr'
    ? indexFR.c2_r
    : "";
    let c3_c = router.locale === 'en'? indexEN.c3_c
    : router.locale === 'fr'
    ? indexFR.c3_c
    : "";
    let c3_r = router.locale === 'en'? indexEN.c3_r
    : router.locale === 'fr'
    ? indexFR.c3_r
    : "";
    return(
        <div>
            <div className='flex items-center align-center space-x-4'>
                <svg xmlns="http://www.w3.org/2000/svg" className="lg:h-8 lg:w-8 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                    <path strokeLinecap="round" strokeLinejoin="round" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                <h3 className="text-xl">{h3}</h3>
            </div>
            <div className='border-b border-slate-400'></div>
            <div className='lg:grid lg:grid-cols-4 lg:gap-4 grid grid-cols-2 gap-2'>
                <Countries country={c1_c} reason={c1_r}/>
                <Countries country={c2_c} reason={c2_r}/>
                <Countries country={c3_c} reason={c3_r}/>
            </div>
        </div>
    )
}