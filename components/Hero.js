import styles from '../styles/Home.module.css'
import Link from 'next/link'
import { useRouter } from 'next/router';
import indexEN from '../i18n/en/index.json'
import indexFR from '../i18n/fr/index.json'

export default function Hero(){
  let router = useRouter();
  let name = router.locale === 'en'? indexEN.name
          : router.locale === 'fr'
          ? indexFR.nom
          : "";
  let projects = router.locale === 'en' ? indexEN.projects
          : router.locale === 'fr'
          ? indexFR.projets
          : "";
  let title = router.locale === 'en' ? indexEN.home_h1
  : router.locale === 'fr'
  ? indexFR.accueil_h1
  : "";
          

  return (
      <div className='lg:snap-start w-screen h-screen flex flex-col items-center justify-center'>
        <div className="bg-white  flex flex-row justify-between items-center py-5 border-solid border-b border-slate-400 w-screen lg:w-10/12 fixed top-0 z-10">
          <div className='flex justify-center align-center items-center space-x-2 ml-4'>
            <Link href='/'>
                <a>{name}</a>
            </Link>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-cyan-500" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
              <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
            </svg>
          </div>
          <Link className='text-xlbg-sky-500/25 p-2 rounded-md' href='/project'>
            <a className=' mr-4 transition ease-in-out duration-500 hover:scale-125 underline decoration-cyan-300 underline-offset-4 decoration-2'>{projects}</a>
          </Link>
        </div>
        <div className={styles.title}>
          <h1 className="lg:text-7xl lg:m-0 lg:font-normal font-bold text-2xl text-center ">{title}</h1>
        </div>
      </div>
  )
}