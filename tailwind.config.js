module.exports = {
  darkMode: 'class',
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'blue-h2': '#1375C6',
        'white-txt-darkmode': '#ededed',
      },
      backgroundImage:{
        'Crypto1': "url('/CryptoProject1.jpg')",
      },
      backdropContrast: {
        90: '.90',
        85: '.85',
        75: '.75',
        70: '0.70'
      }
    },
  },
  plugins: [],
}
